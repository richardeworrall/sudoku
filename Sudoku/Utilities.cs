﻿using System;
using System.Linq;
using System.Text;

namespace Sudoku
{
    public static class Utilities
    {
        public static bool CheckAreEqual(Grid g1, Grid g2)
        {
            if (g1 == null || g2 == null) return false;

            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    if (g1.Values[i, j] != g2.Values[i, j]) return false;
                }
            }

            return true;
        }

        public static Grid Parse(string s)
        {
            var grid = Grid.Empty;

            var numbers = s.Where(c => (c >= '0' && c <= '9') || c == '.')
                .Select(c =>
                    int.TryParse(new string(c, 1), out var i)
                        ? (int?)i
                        : null)
                .GetEnumerator();

            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    numbers.MoveNext();

                    if (numbers.Current.HasValue)
                    {
                        grid.Values[i, j] = numbers.Current.Value;
                    }
                }
            }

            return grid;
        }

        public static string Print(Grid grid)
        {
            var sb = new StringBuilder();

            for (var i = 0; i < 9; i++)
            {
                sb.Append(' ');

                for (var j = 0; j < 9; j++)
                {
                    sb.Append(grid.Values[i, j].HasValue ? $"{grid.Values[i, j].Value} " : ". ");

                    if (j == 2 || j == 5) sb.Append("| ");
                }

                sb.Append('\n');
                if (i == 2 || i == 5) sb.Append("-------|-------|-------\n");
            }

            return sb.ToString();
        }
    }
}