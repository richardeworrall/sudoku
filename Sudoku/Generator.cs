﻿using System;

namespace Sudoku
{
    public static class Generator
    {
        private static readonly Random Random = new Random();

        /// <summary>
        /// A Fisher-Yates shuffle
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array to shuffle in-place</param>
        public static void Shuffle<T>(this T[] array)
        {
            if (array == null) return;

            var n = array.Length;

            for (var i = 0; i < n - 1; i++)
            {
                var j = Random.Next(i, n);

                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }
}