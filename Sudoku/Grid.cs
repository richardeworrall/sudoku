﻿using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
	public class Grid
    {
        public int?[,] Values;

        private Grid()
        {
        }

        public static Grid Empty => new Grid
        {
            Values = new int?[9, 9]
        };

        public bool IsValid()
        {
            for (var i = 0; i < 9; i++)
            {
                var i1 = i;

                var knownRowValues = 
                    Enumerable.Range(0, 9)
                        .Select(j => Values[i1, j])
                        .Where(c => c.HasValue).ToArray();

                if (knownRowValues.Length 
                        != knownRowValues.Distinct().Count())
                    return false;

                var i2 = i;
                var knownColumnValues = 
                    Enumerable.Range(0, 9)
                        .Select(j => Values[j, i2])
                        .Where(c => c.HasValue).ToArray();

                if (knownColumnValues.Length != 
                        knownColumnValues.Distinct().Count())
                    return false;
            }

            for (var i = 0; i < 9; i += 3)
            {
                for (var j = 0; j < 9; j += 3)
                {
                    var knownSubcellValues = new List<int>();

                    for (var k = 0; k < 9; k++)
                    {
                        var xOffset = k % 3;
                        var yOffset = (k / 3);

                        var xSub = i + xOffset;
                        var ySub = j + yOffset;

                        if (Values[xSub, ySub].HasValue) knownSubcellValues.Add(Values[xSub, ySub].Value);
                    }

                    if (knownSubcellValues.Count != knownSubcellValues.Distinct().Count()) return false;
                }
            }

            return true;
        }
    }
}