﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sudoku
{
    public class SparseMatrixBuilder
    {
        private Column Root;

        private Column currentColumn;
        private Node above;

        private Node[] First;
        private Node[] Previous;

        public SparseMatrixBuilder(int nRows)
        {
            Root = new Column();

            currentColumn = Root;

            Previous = new Node[nRows];
            First = new Node[nRows];
        }

        public void AddColumn()
        {
            if (above != null)
            {
                above.D = currentColumn;
                currentColumn.U = above;
            }

            var c = new Column { L = currentColumn };

            currentColumn.R = c;

            currentColumn = c;

            above = currentColumn;
        }

        public void MarkConstraint(int rowNumber)
        {
            var n = new Node
            {
                C = currentColumn,
                Row = rowNumber,
                U = above
            };

            above.D = n;

            above = n;

            if (First[rowNumber] == null) 
            {
                First[rowNumber] = n;
                Previous[rowNumber] = n;
            }
            else
            {
                Previous[rowNumber].R = n;
                n.L = Previous[rowNumber];
                Previous[rowNumber] = n;
            }

            currentColumn.S++;
        }

        public Column Finalize()
        {
            currentColumn.R = Root;
            Root.L = currentColumn;

            above.D = currentColumn;
            currentColumn.U = above;

            for (var i = 0; i < First.Length; i++)
            {
                var f = First[i];
                if (f != null)
                {
                    var p = Previous[i];

                    p.R = f;
                    f.L = p;
                }
            }

            var r = Root;
            Root = null;
            Previous = null;
            First = null;
            return r;
        }
    }

    public static class ExactCover
    {
        public static Grid BuildGrid(IEnumerable<int> rows)
        {
            var g = Grid.Empty;

            foreach (var row in rows)
            {
                var c = ConvertToCoordinateValue(row);

                g.Values[c.Item1, c.Item2] = c.Item3;
            }

            return g;
        }

        private static int ConvertToOffset(int row, int col, int value) 
            => 81 * row + 9 * col + (value - 1);

        private static int ConvertToOffset(Tuple<int, int, int> c) 
            => 81 * c.Item1 + 9 * c.Item2 + (c.Item3 - 1);

        private static Tuple<int, int, int> ConvertToCoordinateValue(int offset) 
            => new Tuple<int, int, int>(offset / 81, offset % 81 / 9, (offset % 9) + 1);

        public static IEnumerable<Tuple<int, int, int>> GetSetValues(Grid g)
        {
            if (g == null) yield break;

            for (var i = 0; i < 9; i++)
            {
                for (var j = 0; j < 9; j++)
                {
                    if (g.Values[i, j].HasValue) yield return Tuple.Create(i, j, g.Values[i, j].Value);
                }
            }
        }

        public static Column BuildSudokuConstraintGrid(IEnumerable<Tuple<int, int, int>> givenValues)
        {
            var g = givenValues as Tuple<int, int, int>[] ?? givenValues.ToArray();

            var builder = new SparseMatrixBuilder(729);

            // Individual Cells
            for (var row = 0; row < 9; row++)
            {
                for (var col = 0; col < 9; col++)
                {
                    builder.AddColumn();

                    for (var val = 1; val < 10; val++)
                    {
                        var offset = ConvertToOffset(row, col, val);

                        builder.MarkConstraint(offset);
                    }
                }
            }

            // Rows
            for (var row = 0; row < 9; row++)
            {
                for (var val = 1; val < 10; val++)
                {
                    builder.AddColumn();

                    for (var col = 0; col < 9; col++)
                    {
                        var offset = ConvertToOffset(row, col, val);

                        builder.MarkConstraint(offset);
                    }
                }
            }

            // Columns
            for (var col = 0; col < 9; col++)
            {
                for (var val = 1; val < 10; val++)
                {
                    builder.AddColumn();

                    for (var row = 0; row < 9; row++)
                    {
                        var offset = ConvertToOffset(row, col, val);

                        builder.MarkConstraint(offset);
                    }
                }
            }

            // Cells
            for (var cell = 0; cell < 9; cell++)
            {
                for (var val = 1; val < 10; val++)
                {
                    builder.AddColumn();

                    var xBase = (cell / 3) * 3;
                    var yBase = (cell % 3) * 3;

                    for (var el = 0; el < 9; el++)
                    {
                        var xOffset = el / 3;
                        var yOffset = el % 3;

                        var offset = ConvertToOffset(xBase + xOffset, yBase + yOffset, val);

                        builder.MarkConstraint(offset);
                    }
                }
            }

            // Given Values
            for (var i = 0; i < g.Length; i++)
            {
                builder.AddColumn();

                var offset = ConvertToOffset(g[i]);

                builder.MarkConstraint(offset);
            }

            return builder.Finalize();
        }
        
        public static IEnumerable<int> Search(Column root)
        {
            var chosenNodes = new Stack<Node>();

            SearchRec(root, chosenNodes);
            
            return chosenNodes.Select(n => n.Row);
        }

        private static bool SearchRec(Column root, Stack<Node> chosenNodes)
        {
            if (root.R == root)
            {
                return true;
            }

            var c = FindMinColumn(root);

            if (c.S == 0) return false;

            Cover(c);   // Working through the possible rows that include column c

            var r = c.D;
            
            while (r != c)
            {
                var j = r.R;

                while (j != r)
                {
                    Cover(j.C);
                    j = j.R;
                }

                chosenNodes.Push(r);

                if (SearchRec(root, chosenNodes)) return true;

                chosenNodes.Pop();

                j = r.L;

                while (j != r)
                {
                    Uncover(j.C);
                    j = j.L;
                }

                r = r.D;
            }
            
            Uncover(c);
            
            return false;
        }

        private static Column FindMinColumn(Column root)
        {
            var cMin = root.R;
            var cCurr = root.R;

            while (cCurr != root)
            {
                if (((Column)cCurr).S < 2) return (Column) cCurr;

                if (((Column)cCurr).S < ((Column)cMin).S) cMin = cCurr;
                cCurr = cCurr.R;
            }

            return (Column) cMin;
        }

        private static void Cover(Column c)
        {
            c.R.L = c.L;
            c.L.R = c.R;

            var x = c.D;

            while (x != c)
            {
                var y = x.R;
                
                while (y != x)
                {
                    y.C.S--;

                    y.D.U = y.U;
                    y.U.D = y.D;

                    y = y.R;
                }

                x = x.D;
            }
        }

        private static void Uncover(Column c)
        {
            var x = c.U;

            while (x != c)
            {
                var y = x.L;

                while (y != x)
                {
                    y.C.S++;

                    y.D.U = y;
                    y.U.D = y;

                    y = y.L;
                }

                x = x.U;
            }

            c.R.L = c;
            c.L.R = c;
        }
    }

    public class Node
    {
        public Column C { get; set; }
        public int Row { get; set; }

        public Node L { get; set; }
        public Node R { get; set; }

        public Node U { get; set; }
        public Node D { get; set; }
    }

    public class Column : Node
    {
        public uint S { get; set; }
    }
}