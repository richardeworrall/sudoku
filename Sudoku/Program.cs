﻿using System;
using System.Diagnostics;

namespace Sudoku
{
    class Program
    {
        public static void Main(string[] args)
        {
            var puzzle =
            @"
             8 . . | . . . | . . .
             . . 3 | 6 . . | . . .
             . 7 . | . 9 . | 2 . .
            -------|-------|-------
             . 5 . | . . 7 | . . .
             . . . | . 4 5 | 7 . .
             . . . | 1 . . | . 3 .
            -------|-------|-------
             . . 1 | . . . | . 6 8
             . . 8 | 5 . . | . 1 .
             . 9 . | . . . | 4 . .
            ";

            Console.WriteLine("Starting...");
            Console.WriteLine();

            var sw = new Stopwatch();

            sw.Start();

            var grid = Utilities.Parse(puzzle);

            sw.Stop();

            var tParse = sw.ElapsedMilliseconds;

            sw.Restart();

            var setValues = ExactCover.GetSetValues(grid);

            var constraints = ExactCover.BuildSudokuConstraintGrid(setValues);

            sw.Stop();

            var tBuildMatrix = sw.ElapsedMilliseconds;

            sw.Restart();

            var rows = ExactCover.Search(constraints);

            var solution = ExactCover.BuildGrid(rows);

            sw.Stop();

            var tSolve = sw.ElapsedMilliseconds;

            Console.Write(Utilities.Print(grid));

            Console.WriteLine();
			Console.WriteLine();

			Console.Write(Utilities.Print(solution));

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine($"Parsed grid in {tParse}ms");
            Console.WriteLine($"Built constraint matrix in {tBuildMatrix}ms");
            Console.WriteLine($"Found solution in {tSolve}ms");

            Console.WriteLine();
			Console.Read();
        }
    }
}